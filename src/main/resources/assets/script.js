$(document).ready(function() {

    $("#register-form").validate({

        rules: {
            username: "required",
            password: {
                required: true,
                minlength: 3
            },
            confirmedPassword: {
                equalTo: "#password"
            }
        },

        messages: {
            username: "Please enter your preferred username",
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 3 characters long"
            },
            confirmedPassword: {
                equalTo: "Passwords dont match"
            }
        },
        submitHandler: function(form) {
            $.post('/signup', {
                username: $("#username").val(),
                password: $("#password").val(),
                confirmedPassword: $("#confirmedPassword").val(),
                job: $("#job").val()
            }).done(function(data) {

                if ($.trim(data) === "success")
                    window.location.href = "/login";
                else
                    $("#messageFromServer").text(data);
            });
        }
    });

    $("#login-form").validate({

        rules: {
            username: "required",
            password: "required"
        },

        messages: {
            username: "Please enter your username",
            password: "Enter Your password"
        },
        submitHandler: function(form) {
           
            $.post('/login', {
                username: $("#username").val(),
                password: $("#password").val(),
            }).done(function(data) {

                if ($.trim(data) === "success")
                    window.location.href = "/person";
                else
                    $("#messageFromServer").html("<h4>"+data+"</h4>")
            });
        }
    });
    $("#logout").click(function(event) {
       $.get('/person/logout', function(data) {
           window.location.href="/login";
       });
    });
});