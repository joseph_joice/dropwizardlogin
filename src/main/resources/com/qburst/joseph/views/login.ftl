<html>
    <head>
        <title>Login</title>
    </head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <body>
        <div class="container">
            <form id="login-form">
                <div class="col-sm-12">
                    <div class="form-group col-sm-offset-4 col-sm-4">
                        <label for="username">Username</label>
                        <input type="username" class="form-control " id="username" name="username" placeholder="Enter Username">
                    </div></div>
                    <div class="col-sm-12">
                        <div class="form-group col-sm-offset-4 col-sm-4">
                            <label for="password">Password</label>
                            <input type="password" class="form-control " name="password" id="password" placeholder="Password">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <button type="submit" class="col-sm-offset-5 col-sm-2 btn btn-default">Submit</button>
                    </div>
                </form>
                <div class="col-sm-12">
                    <span class="label label-danger col-sm-offset-4 col-sm-4 " id="messageFromServer" ></span>
                </div>
            </span>
            <div class="col-sm-12">
            <br><div class="col-sm-offset-4 col-sm-4">Or</div><br> <a class="label label-primary col-sm-offset-4 col-sm-4" href="/signup"><h4>SignUp</h></a>
        </div>
        <script src="//code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="/assets/script.js"></script>
    </body>
</html>