<html>
  <head>
    <!-- Load jQuery and the validate plugin -->
    
    
    
  </head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <body>
    
    
    <div class="container">
      <form id="register-form">
        <div class="col-sm-12">
          <div class="form-group col-sm-offset-4 col-sm-4">
            <label for="username">Username</label>
            <input type="username" class="form-control " id="username" placeholder="Enter Username" name="username"/>
          </div></div>
          <div class="col-sm-12">
            <div class="form-group col-sm-offset-4 col-sm-4">
              <label for="job">Job</label>
              <input type="text" class="form-control " id="job" placeholder="Enter Job" name="job"/>
            </div></div>
            <div class="col-sm-12">
              <div class="form-group col-sm-offset-4 col-sm-4">
                <label for="password">Password</label>
                <input type="password" class="form-control " id="password" placeholder="Password" name="password"/>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group col-sm-offset-4 col-sm-4">
                <label for="confirmedPassword">Confirm Password</label>
                <input type="password" class="form-control " id="confirmedPassword" placeholder="Retype Password" name="confirmedPassword"/>
              </div>
            </div>
            <div class="col-sm-12">
              <button type="submit" class="col-sm-offset-5 col-sm-2 btn btn-default">Submit</button>
            </div>
          </form>
          <div class="col-sm-12">
            <span class="label label-danger col-sm-offset-4 col-sm-4 " id="messageFromServer" ></span>
          </div>
        </span>
        <script src="https://code.jquery.com/jquery-1.11.1.js"></script>
        <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>
        <script src="/assets/script.js"></script>
      </body>
    </html>