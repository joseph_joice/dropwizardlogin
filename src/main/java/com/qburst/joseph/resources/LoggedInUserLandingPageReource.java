package com.qburst.joseph.resources;

import com.qburst.joseph.Utils.UtilitiesClass;
import com.qburst.joseph.db.PersonDao;
import io.dropwizard.jersey.sessions.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qburst.joseph.views.LoggedInUserLandingPageView;

import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.net.URISyntaxException;


/**
 * Created by joseph on 26/4/15.
 */
@Path("/person")
@Produces(MediaType.TEXT_HTML)
public class LoggedInUserLandingPageReource {
    private PersonDao dao;
    private static final Logger logger = LoggerFactory.getLogger(LoggedInUserLandingPageReource.class);

    public LoggedInUserLandingPageReource(PersonDao dao) {
        this.dao = dao;
    }

    @GET
    public Object gotoUsername(@Session HttpSession httpSession) {
        if (httpSession.getAttribute("userName") == null) {
            try {
                logger.info("again tto login");
                return new UtilitiesClass().redirectToURI("/login");
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        } else {
            LoggedInUserLandingPageView view = new LoggedInUserLandingPageView(dao.getPersonByUsername(httpSession.getAttribute("userName").toString()));
            return view;
        }
        return null;
    }

    @GET
    @Path("/logout")
    public String  logout(@Session HttpSession session) {
        session.invalidate();
            return "success";

    }
}
