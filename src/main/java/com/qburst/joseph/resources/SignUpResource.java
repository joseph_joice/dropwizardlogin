package com.qburst.joseph.resources;

import com.qburst.joseph.Utils.UtilitiesClass;
import com.qburst.joseph.db.PersonDao;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qburst.joseph.views.SignUpView;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;


/**
 * Created by joseph on 24/4/15.
 */
@Path("/signup")
public class SignUpResource {
    private PersonDao dao;

    private static final Logger logger = LoggerFactory.getLogger(SignUpResource.class);

    public SignUpResource(PersonDao personDao) {

        this.dao = personDao;


    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public SignUpView getSignUpPage() {
        return new SignUpView();
    }

    @POST
    public String addNewUser(@FormParam("username") String userName, @FormParam("job") String job, @FormParam("password") String password, @FormParam("confirmedPassword") String confirmedPassword) {

        if(userName.trim().isEmpty()||password.trim().isEmpty())
        {
           return "Username or Password is empty";
        }
        else if(userName.contains(" "))
        {
            return  "Username cannot have spaces";
        }
        else if (dao.getPersonByUsername(userName) != null) {
            return "Username already ";

        } else if (userName.length() >= 32) {
            return "Please select a smaller username";
        } else if (userName == null || password == null) {
            return "All fields are compulsory";
        } else if (password.length() < 3) {
            return "Select a larger password";
        } else if (password.equals(confirmedPassword)) {
            boolean dbUpdateStatus = dao.insertUser(userName, UtilitiesClass.convertToHashAndRemoveLastFiveChars(password), job).wasApplied();
            logger.info("mysql returned " + dbUpdateStatus);
            if (dbUpdateStatus == true) {
                try {
                    return "success";
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        } else {
           return "Passwords don't match";

        }
        return "Some Error Occured";


    }

}
