package com.qburst.joseph.resources;

import com.qburst.joseph.Utils.UtilitiesClass;
import com.qburst.joseph.db.PersonDao;
import com.qburst.joseph.models.Person;
import io.dropwizard.jersey.sessions.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qburst.joseph.views.LoginView;

import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.net.URISyntaxException;

/**
 * Created by joseph on 24/4/15.
 */

@Path("/{parameter: |login}")
@Produces(MediaType.TEXT_HTML)
public class LoginResource {
    private PersonDao dao;

    private static final Logger logger = LoggerFactory.getLogger(LoginResource.class);

    public LoginResource(PersonDao dao) {

        this.dao = dao;
        logger.info("created obj");
    }

    @GET
    public Object getPerson(@Session HttpSession httpSession) {
        if (httpSession.getAttribute("userName") != null)
            try {
                return UtilitiesClass.redirectToURI("/person");
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        return new LoginView();

    }

    @POST
    public String checkUserCredentials(@FormParam("username") String userName, @FormParam("password") String password, @Session HttpSession httpSession)  {

        logger.info("username is" + userName + " password is " + password);
        if (userName.trim().isEmpty()||password.trim().isEmpty()) {
            return "Enter Valid Username and Password";
        } else if (dao.getPersonByUsername(userName) == null) {
            return "Invalid Username";
        } else {
            System.out.println("in username password check");
            Person loggedInPerson = dao.getPersonByUsernameAndPassword(userName, UtilitiesClass.convertToHashAndRemoveLastFiveChars(password));
            if (loggedInPerson != null) {
                httpSession.setAttribute("userName", loggedInPerson.getUsername());
                logger.info("redirected");

                    return "success";

            }
            return "Wrong Password";
        }

    }
}
