package com.qburst.joseph; /**
 * Created by joseph on 22/4/15.
 */


import com.bazaarvoice.dropwizard.assets.ConfiguredAssetsBundle;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.mapping.MappingManager;
import com.datastax.driver.mapping.UDTMapper;
import com.qburst.joseph.db.CassandraConnection;
import com.qburst.joseph.db.PersonDao;
import com.qburst.joseph.models.Person;
import com.qburst.joseph.resources.LoggedInUserLandingPageReource;
import com.qburst.joseph.resources.LoginResource;
import com.qburst.joseph.resources.SignUpResource;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;

import io.dropwizard.jdbi.DBIFactory;

import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import org.eclipse.jetty.server.session.SessionHandler;


public class DropWizardLoginApplication extends Application<DropWizardLoginConfiguration> {
    public static void main(String[] args) throws Exception {

        new DropWizardLoginApplication().run(args);

    }



    @Override
    public void initialize(Bootstrap<DropWizardLoginConfiguration> bootstrap) {

        bootstrap.addBundle(new AssetsBundle("/favicon.ico", "/favicon.ico", null, "favicon"));
        bootstrap.addBundle(new ConfiguredAssetsBundle("/assets/", "/dashboard/"));
        bootstrap.addBundle(new AssetsBundle("/assets/script.js", "/assets/script.js", null, "js"));
        bootstrap.addBundle(new ViewBundle());

    }

    @Override
    public void run(DropWizardLoginConfiguration configuration,
                    Environment environment) {
        MappingManager manager = new MappingManager (new CassandraConnection(configuration.getCassandradb().getHost(),configuration.getCassandradb().getKeyspace()).getSession());
       PersonDao dao = manager.createAccessor(PersonDao.class);


        environment.jersey().register(new SignUpResource(dao));
        environment.jersey().register(new LoginResource(dao));
        environment.jersey().register(new LoggedInUserLandingPageReource(dao));
        environment.servlets().setSessionHandler(new SessionHandler());
        environment.jersey().register(new RuntimeExceptionMapper());


    }


}
