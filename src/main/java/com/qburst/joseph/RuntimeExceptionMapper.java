package com.qburst.joseph;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.qburst.joseph.views.ErrorView;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by joseph on 27/4/15.
 */
@Provider
public class RuntimeExceptionMapper implements ExceptionMapper<RuntimeException> {

    private static final Logger logger = LoggerFactory.getLogger(RuntimeExceptionMapper.class);

    public Response toResponse(RuntimeException exception) {

        Response response = Response
                .serverError()
                .entity(new ErrorView(exception))
                .build();
        logger.info("error type",exception.getMessage());
        logger.error("Error Catched", exception);


        return response;
    }
}