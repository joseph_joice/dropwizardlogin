package com.qburst.joseph.db;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Param;
import com.datastax.driver.mapping.annotations.Query;
import com.qburst.joseph.models.Person;


/**
 * Created by joseph on 23/4/15.
 */
@Accessor
public interface PersonDao {




    @Query("select * from person where username = :userName")
    Person getPersonByUsername(@Param("userName") String userName);
    @Query("select * from person where username = :userName and passhash=:passhash")
    Person getPersonByUsernameAndPassword(@Param("userName") String userName,@Param("passhash") String passhash);

   @Query("insert into person (username,passhash,job) values (:name,:pass,:job)")
   ResultSet insertUser(@Param("name") String userName, @Param("pass") String passwordHash, @Param("job") String job);


}
