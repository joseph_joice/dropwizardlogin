package com.qburst.joseph;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by joseph on 4/5/15.
 */
public class Cassandradb {

    private String keyspace;

    public String getKeyspace() {
        return keyspace;
    }

    public void setKeyspace(String keyspace) {
        this.keyspace = keyspace;
    }


    private String host;
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }


}
