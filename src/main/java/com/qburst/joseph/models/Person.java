package com.qburst.joseph.models;

/**
 * Created by joseph on 22/4/15.
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.datastax.driver.mapping.annotations.Table;
import com.datastax.driver.mapping.annotations.PartitionKey;

import java.util.Objects;

@Table(keyspace = "demo", name = "person")
public class Person {
    @PartitionKey
    @JsonProperty("name")
    private String username;
    @JsonProperty("passhash")
    private String passhash;
    @JsonProperty("job")
    private String job;


    public Person() {


    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }

    public String getPasshash() {
        return passhash;
    }

    public void setPasshash(String passwordHash) {
        this.passhash = passwordHash;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;

        final Person that = (Person) o;

        return Objects.equals(this.username, that.username) &&
                Objects.equals(this.passhash, that.passhash);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, passhash, job);
    }
}
