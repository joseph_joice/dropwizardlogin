package com.qburst.joseph.views;

import io.dropwizard.views.View;

/**
 * Created by joseph on 27/4/15.
 */
public class ErrorView extends View {
    public ErrorView(Exception exception) {
        super("errorview.ftl");
    }
}
