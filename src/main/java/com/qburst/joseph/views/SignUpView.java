package com.qburst.joseph.views;

import io.dropwizard.views.View;

/**
 * Created by joseph on 24/4/15.
 */
public class SignUpView extends View {
    String messageFromServer;

    public String getMessageFromServer() {
        return messageFromServer;
    }

    public void setMessageFromServer(String messageFromServer) {
        this.messageFromServer = messageFromServer;
    }

    public SignUpView() {
        super("signup.ftl");
        messageFromServer = "Enter a username and password";
    }

}
