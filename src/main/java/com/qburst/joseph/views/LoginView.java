package com.qburst.joseph.views;

import io.dropwizard.views.View;

/**
 * Created by joseph on 24/4/15.
 */
public class LoginView extends View {
    private String messageFromServer;

    public String getMessageFromServer() {
        return messageFromServer;
    }

    public void setMessageFromServer(String messageFromServer) {
        this.messageFromServer = messageFromServer;
    }

    public LoginView() {
        super("login.ftl");
        messageFromServer = "";
    }
}
