package com.qburst.joseph.views;

import io.dropwizard.views.View;
import com.qburst.joseph.models.Person;

/**
 * Created by joseph on 25/4/15.
 */
public class LoggedInUserLandingPageView extends View {
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    private Person person;

    public LoggedInUserLandingPageView(Person person) {
        super("loggedInUserLandingPage.ftl");
        this.person = person;
    }
}
