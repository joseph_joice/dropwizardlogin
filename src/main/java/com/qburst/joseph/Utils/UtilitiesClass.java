package com.qburst.joseph.Utils;

import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by joseph on 26/4/15.
 */
public class UtilitiesClass {
    public static Response redirectToURI(String newURI) throws URISyntaxException {
        URI uri = new URI(newURI);
        return Response.seeOther(uri).build();
    }

    public static String convertToHashAndRemoveLastFiveChars(String password) {

        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        md.update(password.getBytes());

        byte byteData[] = md.digest();

        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            String hex = Integer.toHexString(0xff & byteData[i]);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        String hashString = hexString.toString();
        hashString = hashString.substring(0, hashString.length() - 5);
        return hashString;
    }
}
